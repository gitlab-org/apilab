const Promise = require('bluebird');
const express = require('express');
const cors = require('cors');
const middleware = Promise.promisify(require('swagger-express-middleware'));
const FileDataStore = require('swagger-express-middleware/lib/data-store/file-data-store');
const Resource = require('swagger-express-middleware/lib/data-store/resource');
const spectacle = require('spectacle-docs');

const initialData = require('./initial-data.json');
const routesFilePath = './routes.yml';

const app = express();
app.use(cors());

const myDB = new FileDataStore('./data/');
myDB.save(Resource.parse(initialData));

console.log('Starting app...');

Promise.all([
    middleware(routesFilePath, app),
    spectacle({
      silent: true,
      specFile: routesFilePath,
      targetFile: 'index.html',
      targetDir: 'dist'
    })
  ])
  .then(([ middleware ]) => {
    app.use(express.static('dist'));

    app.get('/routes.yml', (req, res) => {
      res.sendFile(routesFilePath, { root: './' });
    });

  	app.use(
  		middleware.metadata(),
  		middleware.CORS(),
  		middleware.files(),
  		middleware.parseRequest(),
  		middleware.validateRequest(),
  		middleware.mock(myDB)
  	);
  })
  .catch((err) => {
    console.log('Error', err, err.stack);
    app.get('*', (req, res) => {
      res.status(500).send({
        error: err.message,
        stack: err.stack
      });
    });
  })
  .then(() => {
  	app.listen(8000, function() {
  		console.log('API Lab is now running at http://localhost:8000');
  	});
  })
