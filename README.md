# API Lab

```
npm i
npm start
```

Documentation for the current routes is available at the root (automatically generated).

```
http://localhost:8000/
```

The swagger document is also served for reference.

```
http://localhost:8000/routes.yml
```


## Update Routes/Endpoints

See `routes.yml` ([Swagger](http://swagger.io/specification/) document)


### Initial mock data

See `initial-data.json` (Array of [Resource objects](https://github.com/BigstickCarpet/swagger-express-middleware/blob/master/docs/samples/walkthrough2.md#pre-populated-data))


## Testing

Currently only linting available for Swagger document `npm run lint`


# Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
