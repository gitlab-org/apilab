const gulp = require('gulp');
const SwaggerParser = require('swagger-parser');


gulp.task('lint-routes', () => {
	return SwaggerParser.validate('./routes.yml');
});



gulp.task('lint', gulp.series(
	'lint-routes'
));

gulp.task('default', gulp.series(
	'lint'
));
